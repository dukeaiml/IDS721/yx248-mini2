use tracing_subscriber::filter::{EnvFilter, LevelFilter};use lambda_http::{run, service_fn, Body, Error, Request, RequestExt, Response};

/// This is the main body for the function.
/// Write your code inside it.
/// There are some code example in the following URLs:
/// - https://github.com/awslabs/aws-lambda-rust-runtime/tree/main/examples
async fn calculator(event: Request) -> Result<Response<Body>, Error> {
    let query_params = event.query_string_parameters_ref();

    let x1 = query_params
        .and_then(|params| params.first("x1"))
        .and_then(|value| value.parse::<f64>().ok())
        .unwrap_or(0.0);
        // .and_then(|value| value.parse::<i32>().ok())

    let x2 = query_params
        .and_then(|params| params.first("x2"))
        .and_then(|value| value.parse::<f64>().ok())
        .unwrap_or(0.0);
        // .and_then(|value| value.parse::<i32>().ok())

    let op = query_params
        .and_then(|params| params.first("op"))
        .unwrap_or("+");

    let result = match op {
        "+" => (x1 + x2).to_string(),
        "-" => (x1 - x2).to_string(),
        "x" => (x1 * x2).to_string(),
        "/" => {
            if x2 == 0.0 {
                "Division by zero error".to_string()
            } else {
                (x1 / x2).to_string()
            }
        },
        _ => "Invalid operation".to_string(),
    };

    // let message = format!("\{[x1:{}], [op:{}], [x2:{}], [result:{}]\}", x1, op, x2, result);
    let message = format!("{{\"x1\": {}, \"op\": \"{}\", \"x2\": {}, \"result\": \"{}\"}}", x1, op, x2, result);

    // Return something that implements IntoResponse.
    // It will be serialized to the right response event automatically by the runtime
    let resp = Response::builder()
        .status(200)
        .header("content-type", "text/html")
        .body(message.into())
        .map_err(Box::new)?;
    Ok(resp)
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing_subscriber::fmt()
        .with_env_filter(
            EnvFilter::builder()
                .with_default_directive(LevelFilter::INFO.into())
                .from_env_lossy(),
        )
        // disable printing the name of the module in every log line.
        .with_target(false)
        // disabling time is handy because CloudWatch will add the ingestion time.
        .without_time()
        .init();

    run(service_fn(calculator)).await
}
