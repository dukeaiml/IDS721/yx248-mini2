use serde::{Deserialize, Serialize};
use rusoto_core::Region;
use rusoto_s3::{S3Client, S3, GetObjectRequest};
use csv::ReaderBuilder;
use std::error::Error;
use lambda_http::{run, service_fn, Body, Request, RequestExt, Response};
use tokio::io::AsyncReadExt;
use tracing_subscriber::filter::{EnvFilter, LevelFilter};

#[derive(Debug, Deserialize, Serialize)]
struct Record {
    #[serde(rename = "Date")]
    date: String,
    #[serde(rename = "Product")]
    product: String,
    #[serde(rename = "Price")]
    price: f64,
    #[serde(rename = "Quantity")]
    quantity: i32,
}

async fn query_csv_data(event: Request) -> Result<Response<Body>, Box<dyn Error>> {
    let query_params = event.query_string_parameters_ref();

    // let low_price = query_params
    //     .and_then(|params| params.first("low"))
    //     .and_then(|value| value.parse::<f64>().ok())
    //     .unwrap_or(0.0);

    // let high_price = query_params
    //     .and_then(|params| params.first("high"))
    //     .and_then(|value| value.parse::<f64>().ok())
    //     .unwrap_or(f64::MAX);

    let low_price = match query_params.and_then(|params| params.first("low")) {
        Some(value) => value.parse::<f64>().map_err(|_| "Invalid 'low' parameter. Must be a floating-point number.")?,
        None => 0.0,
    };

    let high_price = match query_params.and_then(|params| params.first("high")) {
        Some(value) => value.parse::<f64>().map_err(|_| "Invalid 'high' parameter. Must be a floating-point number.")?,
        None => f64::MAX,
    };

    let s3_client = S3Client::new(Region::UsEast2);
    let get_req = GetObjectRequest {
        bucket: "ids-721-data".to_string(),
        key: "dataset_sample.csv".to_string(),
        ..Default::default()
    };

    let result = s3_client.get_object(get_req).await?;
    let body = result.body.ok_or_else(|| "S3 object body is empty")?;
    let mut body_reader = body.into_async_read();
    let mut csv_content = Vec::new();
    body_reader.read_to_end(&mut csv_content).await?;

    let mut rdr = ReaderBuilder::new().from_reader(csv_content.as_slice());
    let mut records = Vec::new();
    for result in rdr.deserialize() {
        let record: Record = result?;
        if record.price >= low_price && record.price <= high_price { // Updated field name
            records.push(record);
        }
    }

    let response_body = serde_json::to_string(&records)?;
    Ok(Response::new(response_body.into()))
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error + Send + Sync>> {
    tracing_subscriber::fmt()
        .with_env_filter(
            EnvFilter::builder()
                .with_default_directive(LevelFilter::INFO.into())
                .from_env_lossy(),
        )
        // disable printing the name of the module in every log line.
        .with_target(false)
        // disabling time is handy because CloudWatch will add the ingestion time.
        .without_time()
        .init();

    run(service_fn(query_csv_data)).await
}
