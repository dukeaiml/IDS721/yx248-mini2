# ids721-mini2

## Links
- [Deployed Lambda Function on AWS](https://k4a05m5hfl.execute-api.us-east-2.amazonaws.com)<br>
    If use the url without any parameters, it will default low = 0 and high = double max, which means will show all data.
    If you need to use other parameters, please assign values to variable, for example:<br>
    [https://k4a05m5hfl.execute-api.us-east-2.amazonaws.com/?low=1.2&high=2](https://k4a05m5hfl.execute-api.us-east-2.amazonaws.com/?low=1.2&high=2)

- [Deployed Calcultor Lambda Function on AWS](https://k4a05m5hfl.execute-api.us-east-2.amazonaws.com/calculator)<br>
    If use the url without any parameters, it will default to `0+0=`.
    If you need to use other parameters, please assign values to variable, for example:<br>
    [https://k4a05m5hfl.execute-api.us-east-2.amazonaws.com/calculator?x1=1&x2=2&op=%2B](https://k4a05m5hfl.execute-api.us-east-2.amazonaws.com/calculator?x1=1&x2=2&op=%2B)

- [GitLab Repository](https://gitlab.com/dukeaiml/IDS721/yx248-mini2)

## Installation

Before you begin, ensure you have `Rust`, `brew`, and `Cargo Lambda` installed on your system.

### Rust Installation
1. **Download and Install Rust**
Download the Rust installer: Rust provides an installation script that can be downloaded and executed with a single command. Run the following in your terminal:

``` bash
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```

2. **Configure Environment**
Configure your environment: Once the installation is complete, the script will advise you to add the Rust compiler to your PATH. You can do this by running the following command:

``` bash
source $HOME/.cargo/env
```

3. **Verify Installation**
Verify the installation: After installation, you can verify that Rust is correctly installed by typing:

``` bash
rustc --version
```

### Homebrew Installation
1. **Install Dependencies**
Install Dependencies: Before installing Homebrew, you need to install some dependencies. Run the following command:

``` bash
sudo apt-get update && sudo apt-get install build-essential curl file git
```

2. **Install Homebrew**
Install Homebrew: Run the following command to install Homebrew:

``` bash
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
```

This command downloads and runs the Homebrew installation script.

3. **Add Homebrew to PATH**
Add Homebrew to your PATH: After the installation script is finished, follow the instructions provided in the terminal to add Homebrew to your system's PATH. This typically involves adding some lines to your `.bashrc`, `.zshrc`, or other profile script. For example, you might need to add the following lines to your `~/.bashrc` or `~/.profile` file:
    
``` bash
echo 'eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"' >> /home/$USER/.profile
eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"
```

After adding these lines, you should apply the changes by running:
    
``` bash
source ~/.profile
```

4. **Verify Installation**
Verify the Installation: To verify that Homebrew has been installed correctly, run:

``` bash
brew --version
```

### Cargo Lambda Installation
- You can use Homebrew to install Cargo Lambda on macOS and Linux. Run the following commands on your terminal to add our tap, and install it:

``` bash
brew tap cargo-lambda/cargo-lambda
brew install cargo-lambda
```


## Creating and Testing the Project

### Create a New Project
The new subcommand will help you create a new project with a default template. When that's done, change into the new directory:

``` bash
cargo lambda new new-lambda-project && cd new-lambda-project
```

### Project Structure:
We will get the project structure looks like:

``` bash
root
│
├── src
│ └── main.rs
│
├── Cargo.toml
│
└── .gitignore
```

If we need to add some src or modify some funxtion code, we can go into `src` folder to modify the `main.rs` file or add more addition code file or lib folder and files to meet demand.

### Serve the function locally for testing
Run the Lambda emulator built in with the watch subcommand:

``` bash
cargo lambda watch
```

### Test your function
- The invoke subcommand can send JSON payloads to the function running locally. Payloads are deserialized into strongly typed Rust structs, and the invoke call will fail if the payload doesn't have the right shape.

- If you're starting with a basic function that only receives events with a `command` field in the payload, you can invoke them with the following command:

``` bash
cargo lambda invoke --data-ascii "{ \"command\": \"hi\" }"
```

- If you're starting an HTTP function, you can access it with your browser from the local endpoint: `http://localhost:9000/`. You can also `invoke` HTTP functions with the invoke subcommand, the payload to send will depend if this function receives calls from Amazon API Gateway, Amazon Elastic Load Balancer, or Lambda Function URLs.

- If your function integrates with Amazon API Gateway, you can use one of the payload examples that we provide by using the `--data-example` flag:

``` bash
cargo lambda invoke http-lambda --data-example apigw-request
```

### My simple function

#### Price Filter
Get two boundary `high` and `low` to quary data in the range, and return back a json format message as the response.

##### Price Filter Function Sample
1. **Default Sample**
    ![pricefilter_default](image/pricefilter_default.png)

2. **Param Sample**
    ![pricefilter_sample](image/pricefilter_sample.png)

3. **High Price Error Handle Sample**
    ![pricefilter_error1](image/pricefilter_error1.png)

4. **Low Price Error Handle Sample**
    ![pricefilter_error2](image/pricefilter_error2.png)

#### Calucular
Get two number `x1` and `x2` with a operation symbol `op`, and use these two numbers and operators to do calculations, and return back a json format message as the response.

##### Calcultor Function Sample
1. **Add Operation Sample**:
    ![sample1](image/sample1.png)

2. **Sub Operation Sample**:
    ![sample2](image/sample2.png)

3. **Mult Operation Sample**:
    ![sample3](image/sample3.png)

4. **Divi Operation Sample**:
    ![sample4](image/sample4.png)

5. **Invalid Oper Symbol Error Response**:
![sample_err1](image/sample_err1.png)

6. **Division by Zero Error Response**:
![sample_err2](image/sample_err2.png)


## deploy project

### create IAM
[IAM Web](https://us-east-1.console.aws.amazon.com/iam/home?region=us-east-2#/home)
Create an acess key and get your access key and your secret access key.

### Build the function to deploy it on AWS Lambda
Use the build subcommand to compile your function for Linux systems:

``` bash
cargo lambda build --release
```

### Deploy the function on AWS Lambda
Use the deploy subcommand to upload your function to AWS Lambda. This subcommand requires AWS credentials in your system.

``` bash
cargo lambda deploy
```

- INFO: A default execution role for this function will be created when you execute this command. Use the flag --iam-role if you want to use a predefined IAM role. If you get the error about the role creation, check the error message and add the flag in the error message in your deploy command and try it again.

After deploy successfully you will see:
![AWS Lambda_deploy](image/AWS_Lambda_deploy.png)


### Integrating with Amazon API Gateway

Once the Lambda function is deployed with the necessary IAM permissions, you can set up an HTTP API via the [Amazon API Gateway](https://us-east-2.console.aws.amazon.com/apigateway/main/apis?).

#### Steps for API Gateway Integration:

1. **Create an HTTP API**:
   Navigate to the Amazon API Gateway console and select the option to create a new HTTP API.
   ![API Gateway Creation](image/gateway_build.png)

2. **Add Lambda Integration**:
   Choose 'Add integration', then select the Lambda function you've deployed on AWS.
   ![Add Integration](image/gateway_build_integration.png)

3. **Configure Routes**:
   Define the routes for your API. Each route can be linked to the Lambda function for specific processing.
   - Create the route and specify the integration target (Lambda function).
   - ![Create Route](image/route_create.png)
   - ![Route Details](image/route_create1.png)
   - ![Integration Selection](image/route_integration.png)
   - ![Attach Integration](image/route_attach_integration.png)
   - ![Integration Complete](image/route_integration_finish.png)

4. **Deployment and Stages**:
   You can deploy your API using the default stage or create a new one. Once deployed, your function is accessible through Amazon API Gateway.
   - Navigate to 'Deploy/Stages' in the API Gateway console to find the deployed URL for your function.
   - ![API Gateway Stages](image/gateway_Stages.png)

With these steps, your Lambda function is now integrated with Amazon API Gateway, providing an HTTP endpoint for function invocation.

